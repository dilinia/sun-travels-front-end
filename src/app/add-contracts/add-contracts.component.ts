import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,FormArray, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Contract } from '../models/contract';
import { ContractRoom } from '../models/contractRoom';
import { Hotel } from '../models/hotel';
import { Room } from '../models/room';
import { ContractService } from '../services/contract.service';
import { HotelService } from '../services/hotel.service';
import { RoomService } from '../services/room.service';

@Component({
  selector: 'app-load-contracts',
  templateUrl: './add-contracts.component.html',
  styleUrls: ['./add-contracts.component.css']
})
export class AddContractsComponent implements OnInit {

    contractDetailsForm : FormGroup;
    dynamicForm: FormGroup;

    rooms: ContractRoom[] = [];
    contract: Contract = new Contract();
    hotels: Hotel[] = [];
    roomtypes: Room[] =[];
    hotel = {hotel_id:null,name:'',email:'',telephone_no:''};
    room = {roomtype_id:null,type_name:null};
    roomId: number;
    // hotelId: number;
    buttonDisabled: boolean;

    constructor(private fb: FormBuilder,
                private _contractService: ContractService,
                private _hotelService: HotelService,
                private _roomService: RoomService,
                private _router: Router,
                private _snackBar: MatSnackBar) {}

  ngOnInit(){
    this._hotelService.getHotel().subscribe(
      data => {this.hotels = data});

    this.contract.hotel = this.hotel;

    this.buttonDisabled= true;

    this.dynamicForm = this.fb.group({
      filters: this.fb.array([])
    });

    this.contractDetailsForm = new FormGroup({
      hotelname : new FormControl(null,Validators.required),
      startdate : new FormControl(null,[Validators.required]),
      enddate : new FormControl(null,[Validators.required, this.compareStartDate(this.contract.start_date)]),
      markup : new FormControl(null,[Validators.required,Validators.max(100),Validators.min(1)]),
      price : new FormControl(null,Validators.required),
      no_of_rooms : new FormControl(null,Validators.required),
      maximum :new FormControl(null,Validators.required),
      roomtype :new FormControl(null,Validators.required)
    });

  }

  get f() { return this.contractDetailsForm.controls; }

  createFilterGroup() {
    return this.fb.group({
      filterType: [],
      apiType: []
    });
  }

  addFilterToFiltersFormArray() {
    if(this.hotel.hotel_id==null){
      console.log('hotel id undefined');
    }else{
      this.filtersFormArray.push(this.createFilterGroup());
    }

  }

  removeFilterFromFiltersFormArray(index) {
    this.filtersFormArray.removeAt(index);
  }

  get filtersFormArray() {
    return (<FormArray>this.dynamicForm.get('filters'));
  }

  onSubmit() {
    console.log(this.contractDetailsForm);
  }


  roomNumber(){
    if(this.hotel.hotel_id==null){
      this.nullIdSnackBar();
    }else{
      this._roomService.getRoomByHotel(this.hotel.hotel_id ).subscribe(
        data =>{
          console.log(data);
          this.roomtypes = data;
          console.log(this.roomtypes);
        });

      this.rooms.push({ price:null, no_of_rooms:null, max_adults:null,room:this.room });
    }

  }

  removeRoom(index){
    this.rooms.pop[index];
  }

  addRoom(){
    this.contract.contract_roomtypes = this.rooms;
  }

  saveContract(){
    this._contractService.saveContract(this.contract).subscribe(
      data => {
        console.log('response',data);
      }
    )
    //console.log(this.hotelDetailsForm);
  }

  openSnackBar() {
    this._snackBar.open("New contract added successfully!", 'OK',{duration: 2000});
  }

  nullIdSnackBar() {
    this._snackBar.open("Please select a hotel name!", 'OK',{duration: 2000});
  }

  compareStartDate(startdate: Date): ValidatorFn {

    return (control: AbstractControl): {[key:string]:any} | null => {

      let enddate: Date = control.value;
      startdate = this.contract.start_date;
      if (enddate > startdate) {
        return null;
      }
      else{
        return {'compareStartDate':true};
      }

    }
  }

  compareEndDate(enddate: Date): ValidatorFn {

    return (control: AbstractControl): {[key:string]:any} | null => {

      let startdate: Date = control.value;
      enddate = this.contract.start_date;
      if (enddate > startdate) {
        return null;
      }
      else{
        return {'compareEndDate':true};
      }

    }
  }

}
