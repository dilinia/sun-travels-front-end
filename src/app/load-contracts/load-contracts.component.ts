import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,FormArray, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
import { District } from '../models/district';
import { Hotel } from '../models/hotel';
import { Room } from '../models/room';
import { HotelService } from '../services/hotel.service';
import { RoomService } from '../services/room.service';

@Component({
  selector: 'app-load-contracts',
  templateUrl: './load-contracts.component.html',
  styleUrls: ['./load-contracts.component.css']
})
export class LoadContractsComponent implements OnInit {

    hotelDetailsForm : FormGroup;
    dynamicForm: FormGroup;

    rooms: Room[] = [];
    hotel: Hotel = new Hotel();
    districts: District[] =[];

    constructor(private fb: FormBuilder,
                private _hotelService: HotelService,
                private _roomService: RoomService,
                private _router: Router,
                private _snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this._hotelService.getDistrict().subscribe(
      data => {this.districts = data});

    this.dynamicForm = this.fb.group({
      filters: this.fb.array([])
    });

    this.hotelDetailsForm = new FormGroup({
      name : new FormControl(null,Validators.required),
      branch : new FormControl(null,Validators.required),
      email : new FormControl(null,[Validators.required, Validators.email]),
      telephone_no : new FormControl(null,[Validators.required, Validators.maxLength(10), Validators.pattern('[0-9]*')]),
      typeName : new FormControl(null,Validators.required),
      description : new FormControl(null)
    });

  }

  get f() { return this.hotelDetailsForm.controls; }

  createFilterGroup() {
    return this.fb.group({
      filterType: [],
      apiType: []
    });
  }

  addFilterToFiltersFormArray() {
    this.filtersFormArray.push(this.createFilterGroup());
  }

  removeFilterFromFiltersFormArray(index) {
    this.filtersFormArray.removeAt(index);
  }

  get filtersFormArray() {
    return (<FormArray>this.dynamicForm.get('filters'));
  }

  onSubmit() {
    console.log(this.hotelDetailsForm);
  }

  roomNumber(){
    this.rooms.push({roomtype_id:null, type_name: '', description: '' });
  }

  removeRoom(index){
    this.rooms.pop[index];
  }

  addRoom(){
    this.hotel.room_types = this.rooms;
  }

  saveHotel(){

    this._hotelService.saveHotel(this.hotel).subscribe(
      data => {
        console.log('response',data);
      }
    )
    //console.log(this.hotelDetailsForm);
  }

  openSnackBar() {
    this._snackBar.open("New hotel added successfully!", 'OK',{duration: 2000});
  }


}
