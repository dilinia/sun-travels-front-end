import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadContractsComponent } from './load-contracts.component';

describe('LoadContractsComponent', () => {
  let component: LoadContractsComponent;
  let fixture: ComponentFixture<LoadContractsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadContractsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
