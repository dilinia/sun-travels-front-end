import { Component, OnInit } from '@angular/core';
import { Contract } from '../models/contract';
import { District } from '../models/district';
import { Hotel } from '../models/hotel';
import { ContractService } from '../services/contract.service';
import { HotelService } from '../services/hotel.service';
import { DatePipe } from '@angular/common';
import { StringDecoder } from 'string_decoder';

@Component({
  selector: 'app-search-accomodations',
  templateUrl: './search-accomodations.component.html',
  styleUrls: ['./search-accomodations.component.css']
})
export class SearchAccomodationsComponent implements OnInit {

  districts: District[] =[];
  Branch: string;
  hotels: Hotel[] = [];
  contracts: Contract[] = [];
  HotelId: number;
  checkinDate: Date;
  noOfNights:number;
  noOfRooms:number;
  noOfAdults:number;
  results: any[]=[];
  // availabilty:[{room:string; status:string}];
  availabilty: any[]=[];
  contract: Contract = new Contract();
  availableContracts: Contract[] = [];

  constructor(private _hotelService: HotelService,
              private _contractService: ContractService) { }

  ngOnInit(): void {
    this._hotelService.getDistrict().subscribe(
      data => {this.districts = data});

      this.availabilty = [{room:null,  status:null}];
  }

  // getHotelByBranch(){
  //   this._hotelService.getHotelByBranch(this.Branch).subscribe(
  //     data =>{
  //       this.hotels = data;
  //       console.log('hotels: ',this.hotels);
  //     });
  // }

  // getContractByHotel(hotel_id: number,checkinDate:Date, noOfNights:number){
  //   this.HotelId = hotel_id;
  //   this._contractService.getContractByHotel(this.HotelId ).subscribe(
  //     data =>{
  //       this.contracts = data;
  //       console.log('contracts:',this.contracts);
  //     });
  // }

  getContractByBranch(){
    this._contractService.getContractByBranch(this.Branch ).subscribe(
      data =>{
        this.contracts = data;
        console.log('contracts:',this.contracts);
      });
  }

  checkAvailability(){
    let newCheckin = new Date(this.checkinDate).toISOString()
    newCheckin =newCheckin.slice(0,-1)+"+00:00";

    //this.contract = contract;
    console.log('contracts2:',this.contracts);
    for(let contract of this.contracts){
      this._contractService.checkAvailability(newCheckin, this.noOfNights ,this.noOfRooms ,this.noOfAdults ,contract.start_date ,contract.end_date ,contract.contract_id ,contract.markup).subscribe(
        data =>{
          console.log(data);
          this.availabilty=data;
          console.log(this.availabilty);
            this.availableContracts.push(contract);
            this.results.push(this.availabilty);
        });
    }
    console.log(this.results);
  }

}
