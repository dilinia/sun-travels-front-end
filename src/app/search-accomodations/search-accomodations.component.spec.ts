import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchAccomodationsComponent } from './search-accomodations.component';

describe('SearchAccomodationsComponent', () => {
  let component: SearchAccomodationsComponent;
  let fixture: ComponentFixture<SearchAccomodationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchAccomodationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchAccomodationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
