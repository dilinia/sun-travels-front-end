import { Room } from './room';

export class Hotel{

  hotel_id: number;
  name: string;
  branch: string;
  email: string;
  telephone_no: string;
  room_types: {type_name: string, description: string}[];

  constructor(){

  }

}
