import { ContractRoom } from './contractRoom';
import { Hotel } from './hotel';

export class Contract{

  contract_id: number;
  start_date : Date;
  end_date : Date;
  markup : number;
  hotel: {
    hotel_id: number;
    name: string;
    email:string;
    telephone_no:string;
  }
  contract_roomtypes: {price: number;
                      no_of_rooms: number;
                      max_adults: number;
                      room: {roomtype_id:number;}
                      }[];
}
