import { Component } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sun-travels';
  navLinks: any[];
  activeLinkIndex = -1;
  constructor(private router: Router) {
    this.navLinks = [
        {
            label: "Add Hotels",
            link: './load-contracts',
            index: 0
        },
        {
          label: 'Add Contracts',
          link: './add-contracts',
          index: 1
        },
         {
            label: 'View Contracts',
            link: './view-contracts',
            index: 2
        }, {
            label: 'Search Accomodations',
            link: './search-accomodations',
            index: 3
        },
    ];
}
ngOnInit(): void {
  this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  });
}
}
