import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from '../models/room';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  private getUrl: string = "http://localhost:8080/api/v1/rooms";
  private getRoomUrl: string = "http://localhost:8080/api/v1/roomByHotel";

  constructor(private _httpClient: HttpClient) { }

  saveRoom(room: Room): Observable< Room > {
    return this._httpClient.post<Room>(this.getUrl, room);
  }

  getRoomByHotel(hotel_id:number): Observable<any>{
    const params = { params: new HttpParams({fromString: `hotel_id=${hotel_id}`}) };
    return this._httpClient.get<any>(this.getRoomUrl,params);
  }
}
