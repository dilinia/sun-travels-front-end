import { HttpClient, HttpClientModule, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contract } from '../models/contract';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  private postUrl: string = "http://localhost:8080/api/v1/contracts";
  private getUrl: string = "http://localhost:8080/api/v1/contractById";
  private getRoomUrl: string ="http://localhost:8080/api/v1/contractRoomById";
  private checkContractUrl: string = "http://localhost:8080/api/v1/availability";
  private getContractUrl: string = "http://localhost:8080/api/v1/contractByBranch";

  constructor(private _httpClient: HttpClient) { }

  // saveHotel(contract: Contract): Observable<Contract> {
  //   return this._httpClient.post<Contract>(this.postUrl, hotel);
  // }

  saveContract(contract: Contract): Observable<Contract> {
    return this._httpClient.post<Contract>(this.postUrl, contract);
  }

  getContractByHotel(hotel_id:number): Observable<any>{
    const params = { params: new HttpParams({fromString: `hotel_id=${hotel_id}`}) };
    return this._httpClient.get<any>(this.getUrl,params);
  }

  getContractByBranch(branch:string): Observable<any>{
    const params = { params: new HttpParams({fromString: `branch=${branch}`}) };
    return this._httpClient.get<any>(this.getContractUrl,params);
  }

  getRoomsByContract(contract_id:number): Observable<any>{
    const params = { params: new HttpParams({fromString: `contract_id=${contract_id}`}) };
    return this._httpClient.get<any>(this.getRoomUrl,params);
  }

  checkAvailability(checkinDate:string, noOfNights:number, noOfRooms:number, noOfAdults:number, startDate:Date, endDate:Date, contract_id:number, markup:number) : Observable<any>{
    const params = { params: new HttpParams({fromString: `checkinDate=${checkinDate}&noOfNights=${noOfNights}&noOfRooms=${noOfRooms}&noOfAdults=${noOfAdults}&startDate=${startDate}&endDate=${endDate}&contract_id=${contract_id}&markup=${markup}`}) };
    //const headers ={ HttpHeaders: `checkinDate=${checkinDate}&noOfNights=${noOfNights}&startDate=${startDate}&endDate=${endDate}`};
    return this._httpClient.get(this.checkContractUrl,params);
  }


}
