import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { District } from '../models/district';
import { Hotel } from '../models/hotel';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  private postUrl: string = "http://localhost:8080/api/v1/hotels";
  private getUrl: string = "http://localhost:8080/api/v1/gethotels";
  private getHotelUrl: string = "http://localhost:8080/api/v1/hotelByLocation";
  private getDistrictUrl: string = "http://localhost:8080/api/v1/districts";

  constructor(private _httpClient: HttpClient) { }

  saveHotel(hotel: Hotel): Observable<Hotel> {
    return this._httpClient.post<Hotel>(this.postUrl, hotel);
  }

  getHotel(): Observable<Hotel[]>{
    return this._httpClient.get<Hotel[]>(this.getUrl);
  }

  getDistrict(): Observable<District[]>{
    return this._httpClient.get<District[]>(this.getDistrictUrl);
  }

  getHotelByBranch(branch: string): Observable<any>{
    const params = { params: new HttpParams({fromString: `branch=${branch}`}) };
    return this._httpClient.get<any>(this.getHotelUrl,params);
  }

  // getHotelId(): Observable<any>{
  //   // let params = new HttpParams();
  //   // params = params.append('branch','Colombo');
  //   // params = params.append('name','Cinnamon');
  //   const params = { params: new HttpParams({fromString: "branch=Colombo&name=Cinnamon"}) };
  //   return this._httpClient.get<any>(this.getIdUrl,params);
  // }
}
