import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddContractsComponent } from './add-contracts/add-contracts.component';
import { LoadContractsComponent } from './load-contracts/load-contracts.component';
import { SearchAccomodationsComponent } from './search-accomodations/search-accomodations.component';
import { ViewContractsComponent } from './view-contracts/view-contracts.component';

const routes: Routes = [
  {path:'',redirectTo:'/load-contracts',pathMatch:'full'},
  {path:'load-contracts', component: LoadContractsComponent},
  {path:'add-contracts', component: AddContractsComponent},
  {path:'view-contracts', component: ViewContractsComponent},
  {path:'search-accomodations', component: SearchAccomodationsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
