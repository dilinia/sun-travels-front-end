import { Component, Input, OnInit } from '@angular/core';
import { Contract } from '../models/contract';
import { ContractRoom } from '../models/contractRoom';

import { Hotel } from '../models/hotel';import { ContractService } from '../services/contract.service';
import { HotelService } from '../services/hotel.service';

@Component({
  selector: 'app-view-contracts',
  templateUrl: './view-contracts.component.html',
  styleUrls: ['./view-contracts.component.css']
})
export class ViewContractsComponent implements OnInit {

  hotels: Hotel[] = [];
  contracts: Contract[] = [];
  allrooms: any[] = [];
  rooms: ContractRoom[] = [];
  hotelsByBranch: Hotel = new Hotel();
  hotelId: number;
  contractId: number;

  constructor(private _hotelService: HotelService,
              private _contractService: ContractService
    ) { }

  ngOnInit() {
    this._hotelService.getHotel().subscribe(
      data => {this.hotels = data});

      // this._hotelService.getHotelId().subscribe(
      //   data1 =>{
      //     console.log(data1);
      //     this.hotelsByBranch = data1;
      //     console.log(this.hotelsByBranch[0].name);
      //   });

  }

  // getHotelId(){
  //   this._hotelService.getHotelId().subscribe(
  //     data1 =>{
  //       console.log(data1);
  //       this.hotelsByBranch = data1;
  //       this.hotelId=this.hotelsByBranch[0].hotel_id;
  //     });
  // }

  getContractByHotel(){
    this._contractService.getContractByHotel(this.hotelId ).subscribe(
      data =>{
        console.log(data);
        this.contracts = data;
        console.log(this.contracts);
      });
  }
  getRoomsByContract(contract_id:number){
    this.contractId = contract_id;
    console.log(this.contractId);
    this._contractService.getRoomsByContract(this.contractId).subscribe(
      data =>{
        console.log(data);
        this.rooms = data;
        console.log(this.rooms);
        this.allrooms.push(this.rooms);
      });
  }



}
