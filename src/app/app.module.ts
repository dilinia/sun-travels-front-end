import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './app.material-modules';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LoadContractsComponent } from './load-contracts/load-contracts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import { ViewContractsComponent } from './view-contracts/view-contracts.component';
import { SearchAccomodationsComponent } from './search-accomodations/search-accomodations.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpParams } from '@angular/common/http';
import { AddContractsComponent } from './add-contracts/add-contracts.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoadContractsComponent,
    ViewContractsComponent,
    SearchAccomodationsComponent,
    AddContractsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
